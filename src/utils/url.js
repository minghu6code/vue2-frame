/**
 * url:  starts '/'
 */
const getParentRoutePath = (url) => {
  const urlArr = url.split('/')
  urlArr.pop()

  const parentUrl = urlArr.join('/')

  if (!parentUrl) {
    return '/'
  }

  return parentUrl
}

export { getParentRoutePath }
