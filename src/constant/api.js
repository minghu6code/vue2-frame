
const RESOURCES = {
  users: 'customers',
  tokens: 'tokens'
}

const ERROR_CODE = {
  accessTokenExpired: 40101,
  refreshTokenInvalid: 40102
}

// Used for axios config
const APIS = {
  getUser: userId => {
    return {
      url: `/${RESOURCES.users}/${userId}`,
      method: 'get'
    }
  },
  getToken: (username, password) => {
    return {
      url: `/${RESOURCES.tokens}`,
      method: 'post',
      data: {
        username,
        password
      }
    }
  },
  refreshToken: refreshToken => {
    return {
      url: `/${RESOURCES.tokens}`,
      method: 'post',
      data: {
        refreshToken
      }
    }
  }
}

export { RESOURCES, ERROR_CODE, APIS }
