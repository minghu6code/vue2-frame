import axios from 'axios'
import { mapActions } from 'vuex'

import store from './store'
import user from '@/store/user'
import { TOKEN_PREFIX } from '@/constant/auth'
import { RESOURCES, ERROR_CODE, APIS } from '@/constant/api'

function isNoAuthApi (config) {
  if (config.url === `/${RESOURCES.tokens}`) {
    return true
  }

  return false
}

const instance = axios.create()

instance.defaults.timeout = 60000
instance.defaults.baseURL = 'http://localhost:8989'

instance.interceptors.request.use(config => {
  if (!isNoAuthApi(config)) {
    const accessToken = user.state.accessToken
    config.headers.Authorization = TOKEN_PREFIX + accessToken

    return config
  }

  return config
}, error => Promise.reject(error))

console.log(mapActions(['refreshSession']))

instance.interceptors.response.use(response => {
  return response
}, error => {
  const errResp = error.response

  if (!errResp.data || !errResp.data.error_code) {
    return Promise.reject(error)
  }

  const errData = errResp.data

  if (errData.error_code !== ERROR_CODE.accessTokenExpired || !user.state.refreshToken) {
    return Promise.reject(error)
  }

  // try refresh access token

  return instance
    .request(APIS.refreshToken(user.state.refreshToken))
    .then(refreshTokenResponse => {
      if (!refreshTokenResponse.data.data) {
        return refreshTokenResponse
      }

      const accessToken = refreshTokenResponse.data.data.accessToken
      store.commit('refreshSession', accessToken)

      return instance.request(error.config)
    })
})

export default instance
