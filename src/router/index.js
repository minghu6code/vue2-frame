import App from '../App'

import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Observe from '@/components/Observe'
import Layout from '@/components/Layout'
import Session from '@/components/Session'
import SessionIn from '@/components/SessionIn'
import SessionUp from '@/components/SessionUp'
import Post from '@/components/Post'
import store from '@/store'
import { getParentRoutePath } from '@/utils/url'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  component: App,
  children: [
    {
      path: '',
      redirect: '/hello-world'
    },
    {
      path: '/ob',
      name: 'Observe',
      component: Observe
    },
    {
      path: '/layout',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'post',
          name: 'Post',
          component: Post
        },
        {
          path: '',
          redirect: {name: 'Post'}
        }
      ]
    },
    {
      path: '/hello-world', // 首页
      meta: { requiredAuth: false },
      component: HelloWorld
    },
    {
      path: '/session',
      component: Session,
      children: [
        {
          path: 'in',
          name: 'SessionIn',
          component: SessionIn,
          meta: { requiredAuth: false }
        },
        {
          path: 'up',
          name: 'SessionUp',
          component: SessionUp,
          meta: { requiredAuth: false }
        },
        {
          path: '',
          redirect: {name: 'SessionUp'}
        }
      ]
    }
  ]
}]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(({meta, path, matched}, from, next) => {
  const { requiredAuth = true } = meta
  const inSession = Boolean(store.state.user) // true用户已登录， false用户未登录

  if (requiredAuth && !inSession) {
    return next({ name: 'SessionIn' })
  }

  if (matched.length === 0) {
    next(getParentRoutePath(path))
  }

  next()
})

export default router
