export default {
  state: JSON.parse(localStorage.getItem('user')) || {},
  mutations: {
    createSession (state, user) {
      localStorage.setItem('user', JSON.stringify(user))
    },
    deleteSession (state) {
      localStorage.removeItem('user')
    },
    refreshSession (state, accessToken) {
      const user = state
      user.accessToken = accessToken

      localStorage.setItem('user', JSON.stringify(user))
    }
  },
  actions: {
    createSession ({
      commit
    }, user) {
      commit('createSession', user)
    },
    refreshSession ({
      commit
    }, accessToken) {
      commit('refreshSession', accessToken)
    },
    deleteSession ({
      commit
    }) {
      commit('deleteSession')
    }
  }
}
